package dev.rehm.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LoginController {

    @PostMapping(value="/login", consumes = "application/x-www-form-urlencoded")
    public ResponseEntity<String> login(@RequestParam("username") String username,
                                       @RequestParam("password") String pass){
        // check username and password against db records
        // if credentials are present, return token
        if("admin".equals(username) && "supersecret".equals(pass)){
            return ResponseEntity.ok().header("Authorization", username+"-auth-token")
                    .header("Access-Control-Expose-Headers", "Content-Type, Allow, Authorization").build();
        } else {  // if not, return 401
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
    }


}
