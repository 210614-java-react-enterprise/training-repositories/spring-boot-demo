package dev.rehm.models;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Component
@Entity
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "album_id")
    private int id;

    private String name;

    private boolean hasPhysicalRelease;

    @OneToMany
    private List<Track> tracks;

    public Album() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasPhysicalRelease() {
        return hasPhysicalRelease;
    }

    public void setHasPhysicalRelease(boolean hasPhysicalRelease) {
        this.hasPhysicalRelease = hasPhysicalRelease;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return id == album.id && hasPhysicalRelease == album.hasPhysicalRelease && Objects.equals(name, album.name) && Objects.equals(tracks, album.tracks);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, hasPhysicalRelease, tracks);
    }

    @Override
    public String toString() {
        return "Album{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", hasPhysicalRelease=" + hasPhysicalRelease +
                ", tracks=" + tracks +
                '}';
    }
}
