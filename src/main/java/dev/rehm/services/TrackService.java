package dev.rehm.services;

import dev.rehm.daos.TrackRepository;
import dev.rehm.models.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackService {

    @Autowired
    private TrackRepository trackRepo;

    public List<Track> getAllTracks(){
        return trackRepo.findAll();
    }

    public Track getTrackById(int id){
        return trackRepo.getOne(id);
    }

    public List<Track> getTracksByReleaseYear(int year){
        return trackRepo.findByReleaseYear(year);
    }

    public List<Track> getTracksByArtist(String artist){
        return trackRepo.findByArtist(artist);
    }

    public List<Track> getTracksByReleaseYearAndArtist(int year, String artist){
        return trackRepo.findByArtistAndReleaseYear(artist, year);
    }

    public Track createNewTrack(Track t){
        return trackRepo.save(t);
    }
}
