package dev.rehm.controllers;

import dev.rehm.models.Track;
import dev.rehm.services.TrackService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
public class TrackControllerTest {

    private MockMvc mockMvc;

    @MockBean
    TrackService trackService;

    @Autowired
    TrackController trackController;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(trackController).build();
    }

    @Test
    public void returnAllShouldReturnCorrectItems() throws Exception {
        List<Track> trackList = Arrays.asList(
                new Track(1, "song 1", "artist 1", 2001),
                new Track(2, "song 2", "artist 2", 2002),
                new Track(3, "song 3", "artist 3", 2003));

        doReturn(trackList).when(trackService).getAllTracks();

        this.mockMvc.perform(get("/tracks"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[*].name").value(hasItems("song 1", "song 2", "song 3")));
    }

    @Test
    public void createNewShouldReturnNewItem() throws Exception {
        Track t = new Track(0,"song 4", "artist 4", 1995);
        doReturn(t).when(trackService).createNewTrack(t);

        this.mockMvc.perform(post("/tracks")
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("utf-8")
                    .content("{ \"name\":\"song 4\", \"artist\":\"artist 4\", \"releaseYear\": 1995 }"))
                .andExpect(status().isCreated())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name").value("song 4"));

    }

}
