package dev.rehm.integration;

import dev.rehm.models.Track;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class TrackIntegrationTest {

    @Test
    public void testGetOneShouldReturnCorrectItem(){
        TestRestTemplate restTemplate = new TestRestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "auth-token");
        HttpEntity<Track> request = new HttpEntity<>(headers);
        ResponseEntity<Track> response =  restTemplate.exchange(
                "http://localhost:8082/tracks/2",
                HttpMethod.GET,
                request,
                Track.class);
        Track actualBody = response.getBody();
        Track expectedBody = new Track(2, "Rocky Raccoon", "The Beatles", 1968);
        assertAll(
                ()->assertEquals(expectedBody, actualBody),
                ()->assertEquals(200, response.getStatusCodeValue())
        );
    }

    @Test
    public void testGetOneUnauthorizedReturns401(){
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<Track> response =  restTemplate.getForEntity("http://localhost:8082/tracks/2", Track.class);
        assertEquals(401, response.getStatusCodeValue());
    }
}
