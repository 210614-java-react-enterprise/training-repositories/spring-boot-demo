FROM java:8
EXPOSE 8082
COPY target/spring-boot-demo-1.0-SNAPSHOT.jar .
CMD java -jar spring-boot-demo-1.0-SNAPSHOT.jar